package com.android.examenfinal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.examenfinal.model.Libro;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.ListResourceBundle;

public class Activity_NewLibro extends AppCompatActivity {

    private EditText edtTitle;
    private EditText edtResumen;
    private EditText edtAutor;
    private EditText edtDate;
    private EditText edtShop1;
    private EditText edtShop2;
    private EditText edtShop3;
    private ImageView ivImage;


    private Button btnSave;
    private Button btnImage;

    private String s64Image;
    private String base64img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_libro);

        edtTitle = findViewById(R.id.edtTittle);
        edtResumen = findViewById(R.id.edtResumen);
        edtAutor = findViewById(R.id.edtAutor);
//        edtDate = new Date("")
        edtShop1 = findViewById(R.id.edtTienda1);
        edtShop2 = findViewById(R.id.edtTienda2);
        edtShop3 = findViewById(R.id.edtTienda3);

        btnImage = findViewById(R.id.btnCargarPortada);
        btnSave = findViewById(R.id.btnSaveBoock);

        ivImage = findViewById(R.id.imageViewLibro);

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(Activity_NewLibro.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED){
                    //when permssion is not granted
                    //Request permission
                    ActivityCompat.requestPermissions(Activity_NewLibro.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);
                }else{
                    //when permission is granted
                    //Create Method
                    selectImage();
                    Log.i("MAIN_APP", "Despues de SI:" + s64Image);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tit = edtTitle.getText().toString();
                String resumen = edtResumen.getText().toString();
                String autor = edtAutor.getText().toString();

                Date d = new Date();
                CharSequence s  = DateFormat.format("MMMM d, yyyy ", d.getTime());
                String fPublicacion = s.toString();
                String tienda1 = edtShop1.getText().toString();
                String tienda2 = edtShop2.getText().toString();
                String tienda3 = edtShop3.getText().toString();
                String imagen = s64Image;

                Libro libro = new Libro(tit,resumen,autor,fPublicacion,tienda1,tienda2,tienda3,imagen);
                saveBoock(view,libro);
                back(view);
            }
        });
    }

    //Paso 2
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Chech condition
        if(requestCode == 100 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            //When permission is Granted
            //Call method
            selectImage();
        }else {
            Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_LONG).show();
        }
    }
    //Paso 3
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Chech condition
        if(requestCode==100 && resultCode == RESULT_OK && data != null){
            //When result is ok
            //Initialize uri
            Uri uri = data.getData();
            try {
                //Initialize bitmap
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                //Initialize byte stream
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                //Compress bitmap
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

                //Initialize byte array
                byte[] bytes = stream.toByteArray();
                //Get base64 encoded string
                s64Image = Base64.encodeToString(bytes,Base64.DEFAULT);
                //Set encoded text on text

                byte[] bytes1 = Base64.decode(s64Image, Base64.DEFAULT);
                Bitmap bitmap1 = BitmapFactory.decodeByteArray(bytes,0,bytes1.length);
                //set Image
                ivImage.setImageBitmap(bitmap1);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void selectImage(){
        //clear Previus data
        //imageViewBitmap.setImageBitmap(null);

        //Initialize intent
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        //set type
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent,"Select Image"),100);
    }

    private void saveBoock(View view, Libro boock){
        AppDatabase.getInstance(getApplicationContext()).libroDao().insert(boock);
    }
    public void back(View view){
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}