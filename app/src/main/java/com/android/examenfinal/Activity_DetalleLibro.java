package com.android.examenfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.examenfinal.model.Libro;

public class Activity_DetalleLibro extends AppCompatActivity {

    private TextView edtTitle;
    private TextView edtResumen;
    private TextView edtAutor;
    private TextView edtDate;
    private TextView getEdtAutor;

    private ImageView image;

    private Button btnShop1;
    private Button btnShop2;
    private Button btnShop3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_libro);

        Intent intent = getIntent();
        String titulo = intent.getStringExtra("Titulo");

        Libro libro = AppDatabase.getInstance(getApplicationContext()).libroDao().loadAllByTitle(titulo);

        edtAutor = findViewById(R.id.txtAutorD);
        edtResumen = findViewById(R.id.txtResumenD);
        edtTitle = findViewById(R.id.txtTitleD);
        edtDate = findViewById(R.id.txtFechaP);
        image = findViewById(R.id.imagen);

        btnShop1 = findViewById(R.id.btnUbicacionT1);
        btnShop2 = findViewById(R.id.btnUbicacionT2);
        btnShop3 = findViewById(R.id.btnUbicacionT3);

        edtAutor.setText("Autor: " + libro.autor);
        edtResumen.setText("Resumen: " + libro.resumen);
        edtTitle.setText("Título: " + libro.titulo);
        edtDate.setText("Publiación: " + libro.fechaPubliacion);

        libro.imagen = libro.imagen.replace("data:image/png;base64,","");


        byte[] bytes = Base64.decode(libro.imagen, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);

        image.setImageBitmap(bitmap);


        btnShop1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double [] ubicacion = getLatitudLongitud(libro.tienda1);
                moveToMapps(view,String.valueOf(ubicacion[0]),String.valueOf(ubicacion[1]));
            }
        });
        btnShop2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double [] ubicacion = getLatitudLongitud(libro.tienda2);
                moveToMapps(view,String.valueOf(ubicacion[0]),String.valueOf(ubicacion[1]));
            }
        });
        btnShop3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double [] ubicacion = getLatitudLongitud(libro.tienda3);
                moveToMapps(view,String.valueOf(ubicacion[0]),String.valueOf(ubicacion[1]));
            }
        });
    }
    private double[] getLatitudLongitud(String ubicacion){
        String[] cadenas = ubicacion.split(",");
        double latLong[] = new double[2];
        latLong[0] = Double.parseDouble(cadenas[0]);
        latLong[1] = Double.parseDouble(cadenas[1]);

        return latLong;
    }
    private void moveToMapps(View view, String lat, String longd){
        Intent intent = new Intent(getApplicationContext(),MapsActivity.class);
        intent.putExtra("Latitud",lat);
        intent.putExtra("Longitud",longd);
        startActivity(intent);
    }
}