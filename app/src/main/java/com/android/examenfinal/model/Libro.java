package com.android.examenfinal.model;

import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "Libros")
public class Libro{
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String titulo;
    public String resumen;
    public String autor;
    @ColumnInfo(name = "fecha_publicacion")
    public String fechaPubliacion;
    public String tienda1;
    public String tienda2;
    public String tienda3;
    public String imagen;

    public Libro(String titulo, String resumen, String autor, String fechaPubliacion, String tienda1, String tienda2, String tienda3, String imagen) {
        this.titulo = titulo;
        this.resumen = resumen;
        this.autor = autor;
        this.fechaPubliacion = fechaPubliacion;
        this.tienda1 = tienda1;
        this.tienda2 = tienda2;
        this.tienda3 = tienda3;
        this.imagen = imagen;
    }
}
