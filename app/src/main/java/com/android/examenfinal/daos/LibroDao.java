package com.android.examenfinal.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.android.examenfinal.model.Libro;

import java.util.List;

@Dao
public interface LibroDao {
    @Query("SELECT * FROM Libros")
    List<Libro> getAll();

    @Insert
    void insert(Libro libro);

    @Query("SELECT * FROM Libros WHERE titulo IN (:title)")
    Libro loadAllByTitle(String title);
}
