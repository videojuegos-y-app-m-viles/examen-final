package com.android.examenfinal.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.examenfinal.R;
import com.android.examenfinal.model.Libro;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.PokemonViewHolder> {

    final OnItemClickListener listener;
    List<Libro> libros;

    public interface OnItemClickListener {
        void onItemClick(Libro libro);
    }
    public ItemAdapter(List<Libro> libros, OnItemClickListener onItemClickListener) {
        this.listener = onItemClickListener;
        this.libros = libros;
    }
    @Override
    public PokemonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlibro,parent,false);
        return new PokemonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemAdapter.PokemonViewHolder holder, int position) {

        View view = holder.itemView;
        Libro libro = libros.get(position);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        ImageView portada = view.findViewById(R.id.tvPortadaLibro);

        tvTitle.setText(libro.titulo);

        libro.imagen = libro.imagen.replace("data:image/png;base64,","");

        byte[] bytes = Base64.decode(libro.imagen, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);

        portada.setImageBitmap(bitmap);
        //Picasso.get().load(libro.imagen).into(portada);

        /*
        TextView tvnombre = view.findViewById(R.id.tvNomPokemonss);
        TextView tvtipo = view.findViewById(R.id.tvtipoPokemonss);
        ImageView ivimage = view.findViewById(R.id.tvImagenPokemonI);
        TextView tvlatitud = view.findViewById(R.id.tvlatitudePokemonss);
        TextView tvlogintud = view.findViewById(R.id.tvLongitudePokemonss);
*/

        //Picasso.get().load(pokemon.photo).into(ivimage);
/*
        byte[] bytes = Base64.decode(pokemon.photo, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);

        ivimage.setImageBitmap(bitmap);
        tvlatitud.setText(String.valueOf(pokemon.latitud));
        tvlogintud.setText(String.valueOf(pokemon.longitud));*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(libros.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return libros.size();
    }


    public class PokemonViewHolder extends RecyclerView.ViewHolder {
        public PokemonViewHolder(View itemView) {
            super(itemView);
        }
    }
}

