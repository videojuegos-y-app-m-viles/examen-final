package com.android.examenfinal.services;

import com.android.examenfinal.model.Libro;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ILibrosServices {
    @GET("libros")
    Call<List<Libro>> all();

    @POST("libros")
    Call<Libro> create(@Body Libro libro);
}
