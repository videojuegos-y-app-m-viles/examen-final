package com.android.examenfinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.examenfinal.adapters.ItemAdapter;
import com.android.examenfinal.model.Libro;
import com.android.examenfinal.services.ILibrosServices;

import java.util.ArrayList;
import java.util.List;
import java.util.ListResourceBundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Button btnNewB;
    private Button btnSinc;
    private ItemAdapter itemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rcvPokemons);
        LinearLayoutManager layoutManager;// = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        btnNewB = findViewById(R.id.btnNewBoock);
        btnSinc = findViewById(R.id.btnSincBoocks);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/n00184115/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        btnNewB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Activity_NewLibro.class);
                startActivity(intent);
            }
        });

        btnSinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ILibrosServices services = retrofit.create(ILibrosServices.class);
                services.all().enqueue(new Callback<List<Libro>>() {
                    @Override
                    public void onResponse(Call<List<Libro>> call, Response<List<Libro>> response) {

                        List<Libro> local = AppDatabase.getInstance(getApplicationContext()).libroDao().getAll();
                        List<Libro> newData = new ArrayList<>();
                        if(local.size()>0){
                            for(Libro l : response.body()){
                                for (Libro t : local){
                                    if(!l.titulo.equals(t.titulo) && !newData.contains(l))
                                        newData.add(l);
                                        //AppDatabase.getInstance(getApplicationContext()).libroDao().insert(l);
                                }
                            }
                            for (Libro l : newData){
                                AppDatabase.getInstance(getApplicationContext()).libroDao().insert(l);
                            }
                        }

                       else {
                            for(Libro l : response.body()){
                                AppDatabase.getInstance(getApplicationContext()).libroDao().insert(l);
                            }
                        }
/*
                       List<Libro> remote = response.body();
                        if(remote.size()>0){
                            for(Libro l : response.body()){
                                for (Libro t : local){
                                    if(!l.titulo.equals(t.titulo) && !newData.contains(l))
                                        AppDatabase.getInstance(getApplicationContext()).libroDao().insert(l);
                                }
                            }
                        }
                            for(Libro l : local){
                                services.create(l).enqueue(new Callback<Libro>() {
                                    @Override
                                    public void onResponse(Call<Libro> call, Response<Libro> response) {

                                    }
                                    @Override
                                    public void onFailure(Call<Libro> call, Throwable t) {

                                    }
                                });
                            }
                            */
                    }

                    @Override
                    public void onFailure(Call<List<Libro>> call, Throwable t) {

                    }
                });
            }
        });
        List<Libro> lista = AppDatabase.getInstance(getApplicationContext()).libroDao().getAll();
        itemAdapter = new ItemAdapter(lista, new ItemAdapter.OnItemClickListener(){
            @Override
            public void onItemClick(Libro libro) {
                moveToDetalle(libro);
            }
        });

        recyclerView.setAdapter(itemAdapter);
    }
    private void moveToDetalle(Libro libro){
        Intent description = new Intent(getApplicationContext(), Activity_DetalleLibro.class);
        description.putExtra("Titulo", libro.titulo);
        startActivity(description);
    }
}