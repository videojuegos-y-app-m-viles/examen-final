package com.android.examenfinal;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.android.examenfinal.daos.LibroDao;
import com.android.examenfinal.model.Libro;

@Database(entities = {Libro.class},version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract LibroDao libroDao();
    private static volatile AppDatabase instance;

    static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDatabase.class,Preferences.DATABASE_NAME).allowMainThreadQueries().build();
        }
        return instance;
    }
}
